-- Part 1. Creating a Database in 3rd Normal Form (DML)

-- IMPORTANT: Dataset has not invalid data since tracks and albums are named as it is in playlists. It can be checked by following the playlist_url

-- Track names which are not invalid
SELECT *
FROM SpotifyTopHits
WHERE track_name ~ '^[0-9]+$' OR track_name ~ '^[0-9]+[+][0-9]+$';

-- Albums which are not invalid
SELECT *
FROM SpotifyTopHits
WHERE album ~ '^[0-9]+$'

-- Insert genre data into the genre table, splitting array values and ensuring uniqueness
INSERT INTO top_hit_data.genre (genre_name)
SELECT DISTINCT trim(unnest(string_to_array(regexp_replace(artist_genres, '\[|\]|''', '', 'g'), ','))) AS genre_name
FROM SpotifyTopHits
WHERE artist_genres IS NOT NULL;

-- Check data insertion
SELECT * FROM genre g ;

-- Insert data into the playlist table
INSERT INTO top_hit_data.playlist (playlist_url)
SELECT DISTINCT playlist_url
FROM SpotifyTopHits
WHERE playlist_url IS NOT NULL
AND NOT EXISTS (
    SELECT 1
    FROM top_hit_data.playlist AS p
    WHERE p.playlist_url = SpotifyTopHits.playlist_url
);

SELECT * FROM playlist p;

-- Insert data into the album table
INSERT INTO top_hit_data.album (album_name)
SELECT DISTINCT album
FROM SpotifyTopHits
WHERE album IS NOT NULL
AND NOT EXISTS (
    SELECT 1
    FROM top_hit_data.album AS a
    WHERE a.album_name = SpotifyTopHits.album
);

SELECT * FROM album a;

-- Insert data into the artist table
INSERT INTO top_hit_data.artist (artist_id, artist_name, artist_popularity)
SELECT DISTINCT artist_id, artist_name, artist_popularity
FROM SpotifyTopHits
WHERE artist_id IS NOT NULL
AND NOT EXISTS (
    SELECT 1
    FROM top_hit_data.artist AS ar
    WHERE ar.artist_id = SpotifyTopHits.artist_id
);

SELECT * FROM artist a;

-- Insert data into the track table
INSERT INTO top_hit_data.track (track_id, track_name, track_popularity, year, danceability, energy, key, loudness, mode, speechiness, acousticness, instrumentalness, liveness, valence, tempo, duration_ms, time_signature)
SELECT DISTINCT CONCAT(track_id, '_', year) AS unique_track_id, track_name, track_popularity, year, danceability, energy, key, loudness, mode, speechiness, acousticness, instrumentalness, liveness, valence, tempo, duration_ms, time_signature
FROM SpotifyTopHits
WHERE track_id IS NOT NULL
AND NOT EXISTS (
    SELECT 1
    FROM top_hit_data.track AS t
    WHERE CONCAT(t.track_id, '_', t.year) = CONCAT(SpotifyTopHits.track_id, '_', SpotifyTopHits.year)
);

-- Dataset has two identical tracks, but in different years
SELECT track_id, track_name, "year"  
FROM track t 
WHERE track_name = 'Woman';

SELECT DISTINCT track_id FROM track t;

-- Populate the bridge table (artist_genre) with data from the cleaned dataset
INSERT INTO top_hit_data.artist_genre (artist_id, genre_id)
SELECT DISTINCT st.artist_id, g.genre_id
FROM top_hit_data.spotifytophits AS st
CROSS JOIN LATERAL unnest(string_to_array(trim(both ']' from trim(both '[' from trim(st.artist_genres))), ',')) AS genre_name_alias
JOIN top_hit_data.genre g ON ltrim(REPLACE(g.genre_name, '''', '')) = ltrim(REPLACE(genre_name_alias, '''', ''))
WHERE NOT EXISTS (
    SELECT 1
    FROM top_hit_data.artist_genre ag
    WHERE ag.artist_id = st.artist_id AND ag.genre_id = g.genre_id
);

SELECT * FROM artist_genre ag; 

-- Insert data into the album_track table
INSERT INTO top_hit_data.album_track (album_id, track_id)
SELECT DISTINCT a.album_id, t.track_id
FROM SpotifyTopHits AS st
JOIN top_hit_data.album AS a ON a.album_name = st.album
JOIN top_hit_data.track AS t ON t.track_name = st.track_name
WHERE st.album IS NOT NULL
AND st.track_name IS NOT NULL
AND NOT EXISTS (
    SELECT 1
    FROM top_hit_data.album_track AS at
    WHERE at.album_id = a.album_id AND at.track_id = t.track_id
);

SELECT * FROM album_track at2;

-- Insert data into the artist_track table
INSERT INTO top_hit_data.artist_track (artist_id, track_id)
SELECT DISTINCT a.artist_id, t.track_id
FROM SpotifyTopHits AS st
JOIN top_hit_data.artist AS a ON a.artist_name = st.artist_name
JOIN top_hit_data.track AS t ON t.track_name = st.track_name
WHERE st.artist_name IS NOT NULL
AND st.track_name IS NOT NULL
AND NOT EXISTS (
    SELECT 1
    FROM top_hit_data.artist_track AS at
    WHERE at.artist_id = a.artist_id AND at.track_id = t.track_id
);

SELECT * FROM artist_track at2;

-- Insert data into the playlist_track table
INSERT INTO top_hit_data.playlist_track (playlist_id, track_id)
SELECT DISTINCT p.playlist_id, t.track_id
FROM SpotifyTopHits AS st
JOIN top_hit_data.playlist AS p ON p.playlist_url = st.playlist_url
JOIN top_hit_data.track AS t ON t.track_name = st.track_name
WHERE st.playlist_url IS NOT NULL
AND st.track_name IS NOT NULL
AND NOT EXISTS (
    SELECT 1
    FROM top_hit_data.playlist_track AS pt
    WHERE pt.playlist_id = p.playlist_id AND pt.track_id = t.track_id
);

SELECT DISTINCT track_id FROM playlist_track pt 

-- Part 2. Creating a Denormalized Database (DML)

-- Genre Dimension Table Insertion:
INSERT INTO data_mart.genre_dim (genre_id, genre_name)
SELECT DISTINCT genre_id, genre_name
FROM top_hit_data.genre
WHERE NOT EXISTS (
    SELECT 1
    FROM data_mart.genre_dim AS gd
    WHERE gd.genre_id = top_hit_data.genre.genre_id
);

-- Artist Dimension Table Insertion:
INSERT INTO data_mart.artist_dim (artist_id, artist_name, artist_popularity)
SELECT DISTINCT artist_id, artist_name, artist_popularity
FROM top_hit_data.artist
WHERE NOT EXISTS (
    SELECT 1
    FROM data_mart.artist_dim AS ad
    WHERE ad.artist_id = top_hit_data.artist.artist_id
);

-- Artist Genre Dimension Table Insertion:
INSERT INTO data_mart.artist_genre_dim (artist_id, genre_id)
SELECT DISTINCT ad.artist_id, gd.genre_id
FROM top_hit_data.artist_genre AS ag
JOIN data_mart.artist_dim AS ad ON ag.artist_id = ad.artist_id
JOIN data_mart.genre_dim AS gd ON ag.genre_id = gd.genre_id
WHERE NOT EXISTS (
    SELECT 1
    FROM data_mart.artist_genre_dim AS agd
    WHERE agd.artist_id = ag.artist_id
    AND agd.genre_id = ag.genre_id
);

SELECT * FROM data_mart.artist_dim ad;

-- Playlist Dimension Table Insertion:
INSERT INTO data_mart.playlist_dim (playlist_id, playlist_url)
SELECT DISTINCT playlist_id, playlist_url
FROM top_hit_data.playlist
WHERE NOT EXISTS (
    SELECT 1
    FROM data_mart.playlist_dim AS pd
    WHERE pd.playlist_id = top_hit_data.playlist.playlist_id
);

SELECT * FROM data_mart.playlist_dim pd;

-- Album Dimension Table Insertion:
INSERT INTO data_mart.album_dim (album_id, album_name)
SELECT DISTINCT album_id, album_name
FROM top_hit_data.album
WHERE NOT EXISTS (
    SELECT 1
    FROM data_mart.album_dim AS ad
    WHERE ad.album_id = top_hit_data.album.album_id
);

SELECT * FROM data_mart.album_dim ad 

-- Track Fact Table Insertion:
INSERT INTO data_mart.track_fact (track_id, track_name, track_popularity, year, danceability, energy, key, loudness, mode, speechiness, acousticness, instrumentalness, liveness, valence, tempo, duration_ms, time_signature, artist_id, playlist_id, album_id)
SELECT    
    t.track_id,    
    t.track_name,    
    t.track_popularity,    
    t.year,    
    t.danceability,    
    t.energy,    
    t.key,    
    t.loudness,   
    t.mode,    
    t.speechiness,    
    t.acousticness,    
    t.instrumentalness,    
    t.liveness,    
    t.valence,    
    t.tempo,    
    t.duration_ms,    
    t.time_signature,    
    ard.artist_id,     
    pd.playlist_id,     
    ald.album_id
FROM top_hit_data.track AS t
    JOIN top_hit_data.artist_track art on t.track_id = art.track_id 
    JOIN top_hit_data.playlist_track pr on t.track_id  = pr.track_id 
    JOIN top_hit_data.album_track alt on t.track_id  = alt.track_id 
    JOIN data_mart.playlist_dim AS pd ON pr.playlist_id = pd.playlist_id
    JOIN data_mart.artist_dim AS ard ON art.artist_id = ard.artist_id
    JOIN data_mart.album_dim AS ald ON alt.album_id = ald.album_id
WHERE NOT EXISTS (    
    SELECT 1    
    FROM data_mart.track_fact AS tf    
    WHERE tf.track_id = t.track_id
);

-- Business Questions

-- Business Question: What are the top 10 tracks based on popularity?
-- SQL Query:
WITH TopTracks AS (
    SELECT tf.track_name, tf.track_popularity, ad.artist_name
    FROM data_mart.track_fact tf
    JOIN data_mart.artist_dim ad ON tf.artist_id = ad.artist_id
    ORDER BY tf.track_popularity DESC
    LIMIT 10
)
SELECT * FROM TopTracks;

-- Business Question: What are the top 10 artists based on popularity?
-- SQL Query:
WITH TopArtists AS (
    SELECT artist_name, artist_popularity
    FROM data_mart.artist_dim
    ORDER BY artist_popularity DESC
    LIMIT 10
)
SELECT * FROM TopArtists;

-- Business Question: Which genres are the most popular among tracks?
-- SQL Query:
WITH PopularGenres AS (
    SELECT gd.genre_name, COUNT(DISTINCT tf.track_id) AS num_tracks
    FROM data_mart.genre_dim gd
    JOIN data_mart.artist_genre_dim agd ON gd.genre_id = agd.genre_id
    JOIN data_mart.track_fact tf ON agd.artist_id = tf.artist_id
    GROUP BY gd.genre_name
    ORDER BY num_tracks DESC
    LIMIT 15
)
SELECT * FROM PopularGenres;

-- Business Question: What are the top 5 playlists and their average track popularity?
-- SQL Query:
WITH PlaylistPopularity AS (
    SELECT pd.playlist_url, ROUND(AVG(tf.track_popularity), 2) AS avg_track_popularity
    FROM data_mart.playlist_dim pd
    JOIN top_hit_data.playlist_track pt ON pd.playlist_id = pt.playlist_id
    JOIN data_mart.track_fact tf ON pt.track_id = tf.track_id
    GROUP BY pd.playlist_url
    ORDER BY avg_track_popularity DESC
    LIMIT 5
)
SELECT * FROM PlaylistPopularity;

-- Business Question: What are the top 10 tracks by their duration? 
-- SQL Query:
WITH TrackDuration AS (
    SELECT ROUND(tf.duration_ms / 60000.0, 2) AS duration_minutes, tf.track_name, ad.artist_name
    FROM data_mart.track_fact tf
    JOIN data_mart.artist_dim ad ON tf.artist_id = ad.artist_id
    GROUP BY duration_minutes, tf.track_name, ad.artist_name
    ORDER BY duration_minutes DESC
    LIMIT 10
)
SELECT * FROM TrackDuration;

-- Business Question: How many tracks are included in each playlist?
-- SQL Query:
WITH PlaylistTracks AS (
    SELECT pd.playlist_url, COUNT(DISTINCT tf.track_id) AS num_tracks, tf.year
    FROM data_mart.playlist_dim pd
    JOIN top_hit_data.playlist_track pt ON pd.playlist_id = pt.playlist_id
    JOIN data_mart.track_fact tf ON pt.track_id = tf.track_id
    GROUP BY pd.playlist_url, tf.year 
    ORDER BY num_tracks DESC, year
    LIMIT 5
)
SELECT * FROM PlaylistTracks;

-- Business Question: What are the top 5 tracks based on popularity in each year(from 2010 to 2022)
-- SQL Query:
WITH ranked_tracks AS (
    SELECT DISTINCT 
        tf.track_name,
        tf.track_popularity,
        tf.year,
        ad.artist_name,
        ROW_NUMBER() OVER (PARTITION BY tf.year ORDER BY tf.track_popularity DESC) AS track_rank
    FROM 
        data_mart.track_fact tf
    INNER JOIN 
        data_mart.artist_dim ad ON tf.artist_id = ad.artist_id
    WHERE 
        tf.year BETWEEN 2010 AND 2022
)
SELECT 
    track_name,
    track_popularity,
    year,
    artist_name
FROM 
    ranked_tracks
WHERE 
    track_rank <= 5
ORDER BY 
    year, track_rank;

-- Business Question: Which albums have the highest number of tracks?
-- SQL Query:
WITH AlbumTracks AS (
    SELECT 
        ald.album_name,
        ard.artist_name,
        COUNT(DISTINCT tf.track_id) AS num_unique_tracks
    FROM 
        data_mart.album_dim ald
    INNER JOIN 
        data_mart.track_fact tf ON ald.album_id = tf.album_id
    INNER JOIN 
        data_mart.artist_dim ard ON tf.artist_id = ard.artist_id
    GROUP BY 
        ald.album_name, ard.artist_name
    ORDER BY 
        num_unique_tracks DESC
    LIMIT 5
)
SELECT * FROM AlbumTracks;