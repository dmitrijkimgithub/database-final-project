-- Part 1. Creating Database in 3rd Normal Form (DDL)

CREATE TABLE IF NOT EXISTS SpotifyTopHits (
    playlist_url VARCHAR,
    year INTEGER,
    track_id VARCHAR,
    track_name VARCHAR,
    track_popularity INTEGER,
    album VARCHAR,
    artist_id VARCHAR,
    artist_name VARCHAR,
    artist_genres VARCHAR,
    artist_popularity INTEGER,
    danceability FLOAT,
    energy FLOAT,
    key INTEGER,
    loudness FLOAT,
    mode INTEGER,
    speechiness FLOAT,
    acousticness FLOAT,
    instrumentalness FLOAT,
    liveness FLOAT,
    valence FLOAT,
    tempo FLOAT,
    duration_ms INTEGER,
    time_signature INTEGER
);

copy SpotifyTopHits FROM 'C:/data/SpotifyTopHitPlaylist2010to2022.csv' DELIMITER ',' CSV HEADER;

SELECT * FROM top_hit_data.spotifytophits s ;

CREATE SCHEMA IF NOT EXISTS top_hit_data;

ALTER TABLE public.SpotifyTopHits SET SCHEMA top_hit_data;

-- Tables creation

-- 1. Playlist table
CREATE TABLE IF NOT EXISTS top_hit_data.playlist (
    playlist_id SERIAL PRIMARY KEY,
    playlist_url VARCHAR NOT NULL
);

-- 2. Album table
CREATE TABLE IF NOT EXISTS top_hit_data.album (
    album_id SERIAL PRIMARY KEY,
    album_name VARCHAR NOT NULL 
);

-- 3. Artist table
CREATE TABLE IF NOT EXISTS top_hit_data.artist (
    artist_id VARCHAR PRIMARY KEY,
    artist_name TEXT NOT NULL,
    artist_popularity INTEGER NOT NULL,
    CONSTRAINT chk_artist_popularity CHECK (artist_popularity >= 0 AND artist_popularity <= 100)
);

-- 4. Track table
CREATE TABLE IF NOT EXISTS top_hit_data.track (
    track_id VARCHAR PRIMARY KEY,
    track_name TEXT,
    track_popularity INTEGER NOT NULL,
    year INTEGER NOT NULL,
    danceability FLOAT,
    energy FLOAT,
    key INTEGER,
    loudness FLOAT,
    mode INTEGER,
    speechiness FLOAT,
    acousticness FLOAT,
    instrumentalness FLOAT,
    liveness FLOAT,
    valence FLOAT,
    tempo FLOAT,
    duration_ms INTEGER NOT NULL,
    time_signature INTEGER,
    CONSTRAINT chk_track_popularity CHECK (track_popularity >= 0 AND track_popularity <= 100),
    CONSTRAINT chk_track_danceability CHECK (danceability >= 0 AND danceability <= 1),
    CONSTRAINT chk_track_energy CHECK (energy >= 0 AND energy <= 1),
    CONSTRAINT chk_track_loudness CHECK (loudness >= -60 AND loudness <= 0),
    CONSTRAINT chk_track_speechiness CHECK (speechiness >= 0 AND speechiness <= 1),
    CONSTRAINT chk_track_acousticness CHECK (acousticness >= 0 AND acousticness <= 1),
    CONSTRAINT chk_track_instrumentalness CHECK (instrumentalness >= 0 AND instrumentalness <= 1),
    CONSTRAINT chk_track_liveness CHECK (liveness >= 0 AND liveness <= 1),
    CONSTRAINT chk_track_valence CHECK (valence >= 0 AND valence <= 1),
    CONSTRAINT chk_track_tempo CHECK (tempo >= 0),
    CONSTRAINT chk_track_duration_ms CHECK (duration_ms >= 0),
    CONSTRAINT chk_track_time_signature CHECK (time_signature >= 0 AND time_signature <= 7)
);

CREATE TABLE IF NOT EXISTS top_hit_data.genre (
    genre_id SERIAL PRIMARY KEY,
    genre_name TEXT NOT NULL 
);

-- Create intermediate tables for M:M relationships

-- Artist_Genre table
CREATE TABLE IF NOT EXISTS top_hit_data.artist_genre (
    artist_id VARCHAR,
    genre_id INTEGER,
    PRIMARY KEY (artist_id, genre_id),
    CONSTRAINT fk_artist_genre_artist FOREIGN KEY (artist_id) REFERENCES artist(artist_id),
    CONSTRAINT fk_artist_genre_genre FOREIGN KEY (genre_id) REFERENCES genre(genre_id)
);

-- Playlist_Track table
CREATE TABLE IF NOT EXISTS top_hit_data.playlist_track (
    playlist_id INTEGER,
    track_id VARCHAR,
    PRIMARY KEY (playlist_id, track_id),
    CONSTRAINT fk_playlist_track_playlist FOREIGN KEY (playlist_id) REFERENCES playlist(playlist_id),
    CONSTRAINT fk_playlist_track_track FOREIGN KEY (track_id) REFERENCES track(track_id)
);

-- Album_Track table
CREATE TABLE IF NOT EXISTS top_hit_data.album_track (
    album_id INTEGER,
    track_id VARCHAR,
    PRIMARY KEY (album_id, track_id),
    CONSTRAINT fk_album_track_album FOREIGN KEY (album_id) REFERENCES album(album_id),
    CONSTRAINT fk_album_track_track FOREIGN KEY (track_id) REFERENCES track(track_id)
);

-- Artist_Track table
CREATE TABLE IF NOT EXISTS top_hit_data.artist_track (
    artist_id VARCHAR,
    track_id VARCHAR,
    PRIMARY KEY (artist_id, track_id),
    CONSTRAINT fk_artist_track_artist FOREIGN KEY (artist_id) REFERENCES artist(artist_id),
    CONSTRAINT fk_artist_track_track FOREIGN KEY (track_id) REFERENCES track(track_id)
);

-- Part 2. Creating a Denormalized Database (DDL)

-- Create schema for denormalized model
CREATE SCHEMA IF NOT EXISTS data_mart;

-- Create Genre Dimension Table: genre_dim
CREATE TABLE IF NOT EXISTS data_mart.genre_dim (
    genre_id SERIAL PRIMARY KEY,
    genre_name TEXT NOT NULL
);

-- Create Artist Dimension Table: artist_dim
CREATE TABLE IF NOT EXISTS data_mart.artist_dim (
    artist_id VARCHAR PRIMARY KEY,
    artist_name TEXT NOT NULL,
    artist_popularity INTEGER NOT NULL
);

-- Create Artist Genre Dimension Table: artist_genre_dim 
CREATE TABLE IF NOT EXISTS data_mart.artist_genre_dim ( 
    artist_id VARCHAR,
    genre_id INTEGER,
    PRIMARY KEY (artist_id, genre_id),
    CONSTRAINT fk_artist_genre_dim_artist FOREIGN KEY (artist_id) REFERENCES data_mart.artist_dim(artist_id),
    CONSTRAINT fk_artist_genre_dim_genre FOREIGN KEY (genre_id) REFERENCES data_mart.genre_dim(genre_id)
);

-- Create Playlist Dimension Table: playlist_dim
CREATE TABLE IF NOT EXISTS data_mart.playlist_dim (
    playlist_id SERIAL PRIMARY KEY,
    playlist_url VARCHAR NOT NULL
);

-- Create Album Dimension Table: album_dim
CREATE TABLE IF NOT EXISTS data_mart.album_dim (
    album_id SERIAL PRIMARY KEY,
    album_name VARCHAR NOT NULL
);

-- Create Fact Table: track_fact
CREATE TABLE IF NOT EXISTS data_mart.track_fact (
    track_id VARCHAR,
    track_name TEXT,
    track_popularity INTEGER NOT NULL,
    year INTEGER NOT NULL,
    danceability FLOAT,
    energy FLOAT,
    key INTEGER,
    loudness FLOAT,
    mode INTEGER,
    speechiness FLOAT,
    acousticness FLOAT,
    instrumentalness FLOAT,
    liveness FLOAT,
    valence FLOAT,
    tempo FLOAT,
    duration_ms INTEGER NOT NULL,
    time_signature INTEGER,
    artist_id VARCHAR,
    playlist_id INTEGER,
    album_id INTEGER,
    CONSTRAINT fk_track_artist FOREIGN KEY (artist_id) REFERENCES data_mart.artist_dim(artist_id),
    CONSTRAINT fk_track_playlist FOREIGN KEY (playlist_id) REFERENCES data_mart.playlist_dim(playlist_id),
    CONSTRAINT fk_track_album FOREIGN KEY (album_id) REFERENCES data_mart.album_dim(album_id),
    PRIMARY KEY (track_id, artist_id, playlist_id, album_id)
);